provider "aws" {
    region = "us-east-1"
  
}

resource "aws_instance" "this" {
    ami = "ami-0889a44b331db0194"
    instance_type = "t2.micro"
    #vpc_security_group_ids = ["sg-0120e7c9e70bd2b3a"]
    #subnet_id              = "subnet-eddcdzz4"
    iam_instance_profile = "AmazonSSMRoleForInstancesQuickSetup"
    user_data = file("userdata.sh")
    tags = {
        Name = "FromTerraform"
    }
}
