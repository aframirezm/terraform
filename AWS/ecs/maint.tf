# PROVIDER
provider "aws" {
    region = "us-east-1"
}
resource "aws_instance" "this" {
    ami = "ami-0889a44b331db0194"
    instance_type = "t2.micro"
    #vpc_security_group_ids = ["sg-0120e7c9e70bd2b3a"]
    #subnet_id              = "subnet-eddcdzz4"
    iam_instance_profile = "AmazonSSMRoleForInstancesQuickSetup"
    user_data = file("userdata.sh")
    tags = {
        Name = "FromTerraform"
    }
}
resource "aws_ecr_repository" "this" {
  name                 = "repotest"
  image_tag_mutability = "IMMUTABLE"
#   image_scanning_configuration {
#     scan_on_push = true
#   }
}
resource "aws_ecs_cluster" "this" {
  name = "cluster01"
}
resource "aws_ecs_task_definition" "this" {
  family                   = "this" # Naming our first task
  container_definitions    = <<DEFINITION
  [
    {
      "name": "this",
      "image": "public.ecr.aws/nginx/nginx:perl",
      #"image": "public.ecr.aws/aws-ec2/amazon-ec2-instance-selector:v2.0.3-windows-amd64",
      "essential": true,
      "portMappings": [
        {
          "containerPort": 80,
          "hostPort": 80
        }
      ],
      "memory": 256,
      "cpu": 256
    }
  ]
  DEFINITION
  requires_compatibilities = ["FARGATE"] # Stating that we are using ECS Fargate
  network_mode             = "awsvpc"    # Using awsvpc as our network mode as this is required for Fargate     
  memory                   = 512         # Specifying the memory our container requires
  cpu                      = 256         # Specifying the CPU our container requires
  execution_role_arn       = "arn:aws:iam::164834048231:role/ecsTaskExecutionRole"
  task_role_arn            = "arn:aws:iam::164834048231:role/ecsTaskExecutionRole"
}
resource "aws_ecs_service" "this" {
  name            = "nginx"
  cluster         = aws_ecs_cluster.this.id
  task_definition = aws_ecs_task_definition.this.arn
  desired_count   = 1
  launch_type     = "FARGATE"
  network_configuration {
    security_groups = ["sg-0d9612470d68d14cb"]
    subnets         = ["subnet-0b5d5ca17f546a3ab","subnet-098511ea96af56d57"]  # aws_subnet.private.*.id
    assign_public_ip = true
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.this.id
    container_name   = "this"
    container_port   = 80
  }
  depends_on = [aws_lb_listener.this]
}
resource "aws_lb" "this" {
  name               = "lb-tf"
  internal           = false
  load_balancer_type = "network"
  subnets            = ["subnet-0b5d5ca17f546a3ab","subnet-098511ea96af56d57"]
  tags = {
    Environment = "Test-Terraform"
  }
}
# Definicion target group
resource "aws_lb_target_group" "this" {
  name     = "tg-this"
  port     = "80"
  protocol = "TCP"
  vpc_id   = "vpc-0a228d554aa94c756"
  target_type = "ip"  
  health_check {
    path = "/"
    port = "80"
    protocol = "HTTP"
    healthy_threshold   = 4
    unhealthy_threshold = 2
    timeout             = 2
    interval            = 5
    matcher = "200"
  }
}

resource "aws_lb_listener" "this" {
  load_balancer_arn = aws_lb.this.arn
  port              = "80"
  protocol          = "TCP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.this.arn
  }
}