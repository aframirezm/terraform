resource "aws_instance" "this" {
    ami = "ami-0889a44b331db0194"
    instance_type = "t2.micro"
    #vpc_security_group_ids = ["sg-0120e7c9e70bd2b3a"]
    subnet_id              = aws_subnet.private-us-east-1a.id
    iam_instance_profile = "AmazonSSMRoleForInstancesQuickSetup"
    user_data = file("userdata.sh")
    tags = {
        Name = "FromTerraform"
    }
}